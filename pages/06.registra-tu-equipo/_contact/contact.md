---
title: "Registra tu equipo"
routable: false

container:
  attr:
    id: contacto
  class:
    - page-section
    - sec-contacto
    - sec-form
  is_fluid: false

row_class: ['contact-container']
col_content_class: ['contact-title']
col_form_class: ['contact-form']

cache_enable: false

form:
    name: contact-form
    classes: 'row'
    fields:
        - name: name
          label: Nombre
          placeholder: Nombre Apellidos
          autofocus: off
          autocomplete: on
          outerclasses: col-md-8 offset-md-2
          type: text
          validate:
            required: true
        - name: email
          label: Email
          placeholder: correo@dominio.com
          type: email
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: phone
          label: Teléfono
          placeholder: 10 dígitos
          outerclasses: col-md-8 offset-md-2
          type: text
        - name: empresa
          label: Empresa
          placeholder: Tu empresa
          autofocus: off
          autocomplete: on
          type: text
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: estado
          label: Estado de la república
          placeholder: Estado de la república
          type: select
          options:
            aguascalientes: Aguascalientes
            baja-california: Baja California
            baja-california-sur: Baja California Sur
            campeche: Campeche
            cdmx: Ciudad de México
            chiapas: Chiapas
            chihuahua: Chihuahua
            coahuila: Coahuila
            colima: Colima
            durango: Durango
            mexico: Estado de México
            guanajuato: Guanajuato
            guerrero: Guerrero
            hidalgo: Hidalgo
            jalisco: Jalisco
            michoacan: Michoacán
            morelos: Morelos
            nayarit: Nayarit
            nuevo-leon: Nuevo León
            oaxaca: Oaxaca
            puebla: Puebla
            queretaro: Querétaro
            quintana-roo: Quintana Roo
            slp: San Luis Potosí
            sinaloa: Sinaloa
            sonora: Sonora
            tabasco: Tabasco
            tamaulipas: Tamaulipas
            tlaxcala: Tlaxcala
            veracruz: Veracruz
            yucatan: Yucatán
            zacatecas: Zacatecas
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: model
          label: Modelo
          placeholder: Modelo adquirido
          type: text
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: date
          label: Fecha
          placeholder: dd/mm/yyyy
          type: date
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: serie
          label: Numero de serie (en caso de aplicar)
          placeholder: Numero de serie
          type: text
          outerclasses: col-md-8 offset-md-2
          validate:
            required: false
    buttons:
        - type: submit
        #  classes: negative
          value: Enviar Mensaje

    process:
        - save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
        - email:
            to: isaac.cortes@blackecco.com
            to: contacto@blackeccopos.com
            to_name: 'Isaac Cortes'
            subject: "[Registro de Equipo desde el sitio de BlackEcco POS] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - redirect: /gracias
---

 Si deseas asesoría técnica acerca de como instalar tu equipo, descagar drivers o tienes algún problema de funcionamiento, te invitamos a contactarnos.
