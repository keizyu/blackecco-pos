---
title: "Garantía"
menu: Home
onpage_menu: false
body:
  attr: {}
  class:
    - sec-garantia
    - modular

metadata:
  robots: 'index, follow'

content:
  items: '@self.modular'
  order:
    by: default
    dir: asc
    custom:
      - _garantia
      - _how-to
      - _registra
      - _soluciones
      - _contact
---
