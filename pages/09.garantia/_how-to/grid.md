---
container:
  attr:
    id: how-to
  class:
    - page-section
    - page-section-howto
  is_fluid: false


grid:
  - class:
      - justify-content-around
    cols:
      - class:
          - col-md-12
        content: |
          ## ¿Cómo hago válida mi garantía?
      - class:
          - col-md-6
          - ol-list
        content: |
          1. Te invitamos a visitar nuestra sección de soporte técnico en [www.blackeccopos.com/soporte-tecnico](/soporte-tecnico) en la cual encontraras información de utilidad acerca de drivers, uso del equipo y más.
          2. Si requieres ayuda personalizada puedes contactarnos y un técnico atenderá tus inquietudes para brindarte una solución. Teléfono: 01 800 8012667 Ext 401 / [soporte@blackeccopos.com](mailto:soporte@blackeccopos.com). Nuestros horarios de atención son de lunes a viernes de 9:00am a 6:00pm.
          3. También es posible comunícate con tu distribuidor para que este te ayude en el proceso de garantía y/o cambio físico en caso de aplicar para garantía exprés.
          4. Si tienes alguna duda o comentario no dudes en comunicarte con nosotros en [contacto@blackeccopos.com](mailto:contacto@blackeccopos.com)

          *No intentes reparar el equipo por cuenta propia pues cualquier intervención no autorizada puede hacerte perder la garantía. Recuerda que el servicio de garantía cubre defectos de fábrica y no el uso inadecuado de los equipos.
      - class:
          - col-md-12
        template: 'partials/grid/cel-image.twig'
        content:
          media:
            url: 'theme://images/temp/garantia-pasos.png'
          content: ''


---
