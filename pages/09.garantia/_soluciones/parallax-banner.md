---
container:
  attr:
    id: soluciones-banner
  class:
    - page-parallax-soluciones
    - parallax-banner
  is_fluid: false
---
![](logo-bepos.svg?cropResize=300,300)
## <span> Brindamos <strong>soluciones en tecnología</strong> a la medida de su negocio.</span>
