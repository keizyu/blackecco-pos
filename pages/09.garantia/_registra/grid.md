---
container:
  attr:
    id: black-banner
  class:
    - page-section
    - page-section-black
  is_fluid: false


grid:
    -
        class:
          - justify-content-center
        cols:
            -
              class:
                - col-md-9
              content: |
                ## <span> registra tu equipo </span>
                [registra tu equipo](/registra-tu-equipo?classes=button)
---