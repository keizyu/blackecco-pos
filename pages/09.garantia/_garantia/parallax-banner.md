---
container:
  attr:
    id: parallax-banner
  class:
    - page-parallax-garantia
    - parallax-banner
  is_fluid: false
---

## garantía express
## <span> si tu equipo falla lo reponemos en 24 hrs </span>
Por que sabemos que la productividad de tu negocio no puede detenerse, ponemos a tu disposición el servicio de garantia express. Previo a aplicar tu garantía, te recomendamos contactar a soporte técnico.
