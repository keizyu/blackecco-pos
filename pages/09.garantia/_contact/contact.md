---
# title: "Contacto"
routable: false

container:
  attr:
    id: contacto
  class:
    - page-section
    - sec-contacto
    - sec-form
  is_fluid: false

row_class: ['contact-container']
col_content_class: ['contact-title']
col_form_class: ['contact-form']

cache_enable: false

form:
    name: contact-form
    classes: 'row'
    fields:
        - name: name
          label: Nombre
          placeholder: Nombre Apellidos
          autofocus: off
          autocomplete: on
          outerclasses: col-md-8 offset-md-2
          type: text
          validate:
            required: true
        - name: empresa
          label: Empresa
          placeholder: Tu empresa
          autofocus: off
          autocomplete: on
          type: text
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: email
          label: Email
          placeholder: correo@dominio.com
          type: email
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true
        - name: phone
          label: Teléfono
          placeholder: 10 dígitos
          outerclasses: col-md-8 offset-md-2
          type: text
        - name: description
          label: Mensaje
          placeholder: Por favor describe cómo podemos ayudarte
          type: textarea
          outerclasses: col-md-8 offset-md-2
          validate:
            required: true

        # - name: step-2
        #   title: ¿Dónde y cuándo?
        #   outerclasses: col-md-12
        #   type: sectitle
        #
        # - name: date
        #   label: Fecha
        #   placeholder: dd/mm/yyyy
        #   type: date
        #   outerclasses: col-md-4
        #   validate:
        #     required: true
        #
        # - name: place
        #   label: Lugar
        #   placeholder: Lugar
        #   type: text
        #   outerclasses: col-md-8
        #   validate:
        #     required: true

    buttons:
        - type: submit
        #  classes: negative
          value: Enviar Mensaje

    process:
        - save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
        - email:
            to: isaac.cortes@blackecco.com
            to: contacto@blackeccopos.com
            to_name: 'Isaac Cortes'
            subject: "[Contacto desde el sitio de BlackEcco POS] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - redirect: /gracias
---

### contáctanos
 Si deseas obtener ayuda de forma personalizada o suscribirte a nuestro boletín de novedades, no dudes en contactarnos, tu opinión es importante para nosostros.
