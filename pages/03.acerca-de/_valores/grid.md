---
container:
  attr:
    id: acerca-de
  class:
    - page-section
    - sec-acerca-de
  is_fluid: false

grid:
    -
        class: []
        cols:
            -
              class:
                - col-md-4
                - col-sm-6
                - usp
                - usp-1
              content: |
                ### Confianza
            -
              class:
                - col-md-4
                - col-sm-6
                - usp
                - usp-2
              content: |
                ### Respeto
            -
              class:
                - col-md-4
                - col-sm-6
                - usp
                - usp-3
              content: |
                ### Innovación

---

## nuestros valores
