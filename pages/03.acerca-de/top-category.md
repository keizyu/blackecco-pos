---
title: "Acerca de"
menu: Home
onpage_menu: false
body:
  attr: {}
  class:
    - sec-acerca
    - modular

metadata:
  robots: 'index, follow'

content:
  items: '@self.modular'
  order:
    by: default
    dir: asc
    custom:
      - _top
      - _mision
      - _compromiso
      - _valores
      - _soluciones
      - _contact
---
