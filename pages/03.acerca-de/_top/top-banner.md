---
container:
  attr:
    id: top
  class:
    - page-section-top
    - page-top-acerca
  is_fluid: false
---

## acerca de
<div class="wow fadein">Blackecco POS es una empresa internacional dedicada a la fabricación y al comercio de soluciones en hardware para “Puntos de Venta”. Nuestro enfoque es ofrecer soluciones innovadoras con alta calidad y rendimiento en impresoras térmicas, lectores de códigos, cajones de dinero, PDV y más. Nuestros socios de negocio son los principales mayoristas de tecnología con quienes trabajamos de la mano para llevar soluciones a todo el territorio mexicano. </div>
