---
container:
  attr:
    id: mision
  class:
    - page-section
    - page-section-mision
  is_fluid: false


grid:
  - class:
      - justify-content-around
    cols:
      - class:
          - col-md-6
        content: |
          ## nuestra misión

          <div class="wow fadein">Proveer de soluciones en PDV a nuestros clientes, con quienes buscamos crear relaciones a largo plazo para aumentar la eficiencia y productividad de sus empresas.</div>
---
