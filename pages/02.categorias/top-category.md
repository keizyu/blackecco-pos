---
title: "Categorías"
menu: Home
onpage_menu: false
body:
  attr: {}
  class:
    - sec-categorias
    - modular

metadata:
  robots: 'index, follow'

content:
  items: '@self.modular'
  order:
    by: default
    dir: asc
    custom:
      - _top
      - _categorias
---
