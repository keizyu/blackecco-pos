---
title: 'Display BED701'
product:
    sku: Y10008002
    name: 'Display BED701'
    specs:
        Modelo: BED701
        'Tamaño de la pantalla': '7" Diagonal '
        Interface: USB
        Resolución: 800x480
        'Tipo de alimentación': 'USB 5V'
        'Área visible': '154 x 86 mm'
        Aspecto: '16:09'
        'Tipo de panel': 'Digital. TFT-LCD de alto brillo '
        'Luz trasera': LED
        Luminancia: '450 Nite'
        'Tiempo de respuesta': '25 ms'
        'Ángulo de visión': '350° Horizontal, 60° Vertical '
        'Temperatura de funcionamiento': '-20°c a -45°c'
        'Tipo de base': '2 Secciones ajustables'
    manual: manual_pd700.pdf
    
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    bullet: Displaylink
    pdf: ficha_display_BED701.pdf
taxonomy:
    product:
        - search-enabled
    category: lector-de-codigo-de-barras
    tag:
        - lector-de-codigo-de-barras
swiper:
    as_bkg: true
    container:
        class: {  }
        attr: {  }
    nav:
        prev:
            class:
                - ico
                - ico-md
            attr:
                data-ico: '&#xE002;'
        next:
            class:
                - ico
                - ico-md
            attr:
                data-ico: '&#xE003;'
creator: dern
---

Mayor eficiencia gracias a su velocidad y su lector ligero.

| Modelo | PD700-1 |
| ------ | ----------- |
| Tamaño de la pantalla   | 7" Diagonal |
| Tipo   | Displaylink |
| Interface    | USB |
| Resolución    | 800x480 |
| Aspecto    | 16:09 |
| Tipo de panel    | Digital. TFT-LCD de alto brillo |
| Tipo de base    | 2 Secciones ajustables |
