---
title: 'Cajón BEF4617'
product:
    sku: Y10008003
    name: 'Cajón BEF4617'
    bullet: 'Cajón de dinero'
    specs:
        'Modelo'                        : 'BEF4617'
        'Color'                         : 'Negro'
        'Materiales'                    : 'Acero laminado'
        'Tipo de acabado'               : 'Resistente al polvo'
        'Compartimentos'                : '6 Billetes / 8 Monedas'
        'Detalle del compartimento'     : 'Compartimento removible y ajustable'
        'Dimensiones'                   : '460W x 170D x 100H mm'
        'Interface'                     : 'RJ11'
        'Operaciones'                   : '1.000.000'
        'Seguridad'                     : 'Llave con 2 posiciones de seguridad'
        'Slot de medios'                : '1 Frontal'
        'Micro interruptor'             : 'Sí'
        'Peso'                          : '4.3 kg'
    pdf: 'ficha_cajones_BEF4617.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_cajones.pdf
    

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: cajones-de-dinero
  tag:
    - cajones-de-dinero
creator: dern
---

Diseño innovador ideal para espacios reducidos, alta calidad y flexibilidad.

| Modelo | BEF4617 |
| ------ | ----------- |
| Compartimientos   | 6 Billetes / 8 Monedas |
| Dimensiones   | 460W x 170D x 100H mm |
| Interface    | RJ11 |
| Operaciones    | 1.000.000 |
| Seguridad    | Llave con 2 posiciones de seguridad |
| Slot de medios | 1 Frontal |
