---
title: 'Cajón BE4B8M90'
product:
    sku: Y10008000
    name: 'Cajón BE4B8M90'
    bullet: 'Cajón de dinero'
    specs:
        'Modelo'                        : 'BE4B8M90'
        'Color'                         : 'Negro'
        'Materiales'                    : 'Acero laminado'
        'Tipo de acabado'               : 'Resistente al polvo'
        'Compartimentos'                : '4 Billetes / 8 Monedas'
        'Detalle del compartimento'     : 'Compartimento de monedas removible y ajustable'
        'Dimensiones'                   : '330W x 330D x 90H mm'
        'Interface'                     : 'RJ11'
        'Operaciones'                   : '1.000.000'
        'Seguridad'                     : 'Llave con 3 posiciones de seguridad'
        'Slot de medios'                : '2 Frontales'
        'Micro interruptor'             : 'Sí'
        'Peso'                          : '4.5kg'
    pdf: 'ficha_cajones_BE4B8M90.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_cajones.pdf


swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: cajones-de-dinero
  tag:
    - icajones-de-dinero
creator: dern
---

El mejor cajón para cubrir las necesidades de todo tipo de negocio.

| Modelo | BE4B8M90 |
| ------ | ----------- |
| Compartimientos   | 4 Billetes / 8 Monedas |
| Dimensiones   | 330W x 330D x 90H mm |
| Interface    | RJ11 |
| Operaciones    | 1.000.000 |
| Seguridad    | Llave con 3 posiciones de seguridad |
| Slot de medios | 2 Frontales |
