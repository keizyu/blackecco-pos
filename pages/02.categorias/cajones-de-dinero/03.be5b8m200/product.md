---
title: 'Cajón BE4B8M200'
product:
    sku: Y10008002
    name: 'Cajón BE4B8M200'
    bullet: 'Cajón de dinero'
    specs:
        'Modelo'                        : 'BE4B8M200'
        'Color'                         : 'Negro'
        'Materiales'                    : 'Acero laminado'
        'Tipo de acabado'               : 'Resistente al polvo'
        'Compartimentos'                : '5 Billetes / 8 Monedas'
        'Detalle del compartimento'     : 'Compartimento de monedas removible y ajustable'
        'Dimensiones'                   : '410W x 415D x 100H mm'
        'Interface'                     : 'RJ11'
        'Operaciones'                   : '2.000.000'
        'Seguridad'                     : 'Llave con 3 posiciones de seguridad'
        'Slot de medios'                : '1 Frontal'
        'Micro interruptor'             : 'Sí'
        'Peso'                          : '7.5 kg'
    pdf: 'ficha_cajones_BE5B8M200.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_cajones.pdf
    

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: cajones-de-dinero
  tag:
    - cajones-de-dinero
creator: dern
---

Cajón robusto de alta calidad y durabilidad.

| Modelo | BE4B8M200 |
| ------ | ----------- |
| Compartimientos   | 5 Billetes / 8 Monedas |
| Dimensiones   | 410W x 415D x 100H mm |
| Interface    | RJ11 |
| Operaciones    | 2.000.000 |
| Seguridad    | Llave con 3 posiciones de seguridad |
| Slot de medios | 1 Frontal |
