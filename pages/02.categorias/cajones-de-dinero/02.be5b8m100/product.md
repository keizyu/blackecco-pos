---
title: 'Cajón BE4B8M100'
product:
    sku: Y10008001
    name: 'Cajón BE4B8M100'
    bullet: 'Cajón de dinero'
    specs:
        'Modelo'                        : 'BE4B8M100'
        'Color'                         : 'Negro'
        'Materiales'                    : 'Acero laminado'
        'Tipo de acabado'               : 'Resistente al polvo'
        'Compartimentos'                : '5 Billetes / 8 Monedas'
        'Detalle del compartimento'     : 'Compartimento de monedas removible y ajustable'
        'Dimensiones'                   : '410W x 415D x 100H mm'
        'Interface'                     : 'RJ11'
        'Operaciones'                   : '1.000.000'
        'Seguridad'                     : 'Llave con 3 posiciones de seguridad'
        'Slot de medios'                : '2 Frontales'
        'Micro interruptor'             : 'Sí'
        'Peso'                          : '6.6 kg'
    pdf: 'ficha_cajones_BE5B8M100.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_cajones.pdf
    

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: cajones-de-dinero
  tag:
    - cajones-de-dinero
creator: dern
---

Cajón de diseño clásico, pensado para cubrir las necesidades de todo punto de venta.

| Modelo | BE4B8M100 |
| ------ | ----------- |
| Compartimientos   | 5 Billetes / 8 Monedas |
| Dimensiones   | 410W x 415D x 100H mm |
| Interface    | RJ11 |
| Operaciones    | 1.000.000 |
| Seguridad    | Llave con 3 posiciones de seguridad |
| Slot de medios | 2 Frontales |
