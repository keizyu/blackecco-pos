---
container:
  attr:
    id: categorias
  class:
    - page-section
    - sec-categorias
  is_fluid: true

grid:          
    - class: []
      cols:
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              link:
                url: '/categorias/impresoras-termicas'
              media:
                url: 'theme://images/temp/cat-1.jpg'
              content: '##### impresoras térmicas'             
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              link:
                url: '/categorias/cajones-de-dinero'
              media:
                url: 'theme://images/temp/cat-2.jpg'
              content: '##### cajones de dinero'
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              link:
                url: '/categorias/scanners'
              media:
                url: 'theme://images/temp/cat-3.jpg'
              content: '##### scanners'
          -
            class:
              - cat
              - col-md-3
              - col-sm-6
            template: 'partials/grid/cel-image.twig'
            content:
              link:
                url: '/categorias/display'
              media:
                url: 'theme://images/temp/cat-4.jpg'
              content: '##### Terminal PDV'

---
 Selecciona la categoria de tu interés para acceder a todo el contenido que tenemos para ti. <br>
 Imágenes, fichas técnicas, drivers y más.
