---
title: 'Scanner BESC1'
product:
    sku: Y10008002
    name: 'Scanner BESC1'
    bullet: 'Lector de código de barras'
    specs:
        'Modelo'                            : 'BESC1'
        'Lectura de láser'                  : 'Omnidireccional 5 campos de 4 líneas paralelas'
        'Velocidad de lectura'              : '1500/s'
        'Interface'                         : 'USB - Cable extraíble '
        'Visible'                           : '650nm'
        'Distancia de lectura'              : 'Corta'
        'Color'                             : 'Negro'
        'Base'                              : 'Extraíble'
        'Contraste de impresión mínimo'     : '32%'
        'Indicadores'                       : 'Beeper, LED'
        'Capacidad de decodificación'       : 'EAN-8, EAN-13, UPC-A, UPC-E, Code 39, Code 93, Code 128, EAN 128, ISBN,ISSN, Codabar, Code 25 non-interleaved, Code 25 interleaved , Matrix 2 of 5, MSI, PostBarcode, etc'
    pelec:
        'Volaje'                            : '5VCD±12%  1.6W'
        'Tipo de láser'                     : 'Clase 2 '
        'Color de Led'                      : 'Red and Blue'
    pfis:
        'Peso'                              : '819 g'
        'Dimensiones'                       : '115 x 110 x 152 mm (Largo, Ancho, Alto)'
    dfun:
        'Material y resistencia'            : 'Resistente a caídas menores a 1 metro '
        'Temperatura de operación'          : '0℃~40℃ '
        'Temperatura en reposo'             : '-40℃~60℃'
        'Normas'                            : 'IP54 Resistente a partículas contaminantes en el aire '
    pdf: 'ficha_lector-BESC1.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_besc1.pdf
    

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: lector-de-codigo-de-barras
  tag:
    - lector-de-codigo-de-barras
creator: dern
---

Nuestra solución más robusta para los más exigentes que buscan mejorar su productividad y aumentar la velocidad de las operaciones.

| Modelo | BESC1 |
| ------ | ----------- |
| Lectura de láser   | Omnidireccional 5 campos de 4 líneas paralelas |
| Velocidad de lectura   | 1500/s |
| Distancia de lectura    | Corta |
| Interface    | USB - Cable extraible |
| Base    | Extraíble |
