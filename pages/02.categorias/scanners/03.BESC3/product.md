---
title: 'Scanner BESC3'
product:
    sku: Y10008003
    name: 'Scanner BESC3'
    bullet: 'Lector de código de barras'
    specs:
        'Modelo'                            : 'BESC3'
        'Lectura de láser'                  : 'Bi-direccional'
        'Velocidad de lectura'              : '100/s'
        'Interface'                         : 'USB - Cable extraíble '
        'Visible'                           : '650nm'
        'Distancia de lectura'              : '2.5 ~ 600 mm (100% UPC/EAN)'
        'Color'                             : 'Gris'
        'Base'                              : 'Ajustable con lector removible. '
        'Contraste de impresión mínimo'     : '30%'
        'Indicadores'                       : 'LED'
        'Capacidad de decodificación'       : 'UPC/EAN, UPC/EAN with supplementals, UCC/EAN128, code 39, code 39 full ASCII, code 39 trioptic, code 128, code 128 full ASCII, codabar, interleaved 2 of 5,discrete 2 of 5, code 93, MSI, code 11, ATA, RSS variants, Chinese 2 of 5…'
        'Parámetros eléctricos'             : ''
        'Volaje'                            : 'DC+5V+/-5%'
        'Tipo de láser'                     : 'Clase 2'
        'Color de Led'                      : 'Inclinación 45° Elevación 60°'
        'Parámetros físicos'                : ''
        'Peso'                              : '129 g'
        'Dimensiones'                       : '165 x 63 x 87 mm (Largo, Ancho, Alto)'
        'Detalle de funcionamiento'         : ''
        'Material y resistencia'            : 'NA'
        'Temperatura de operación'          : '0℃~40℃ '
        'Temperatura en reposo'             : '-40℃~60℃'
        'Normas'                            : 'NA'
    pdf: 'ficha_lector-BESC3.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_besc3.pdf
    

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: lector-de-codigo-de-barras
  tag:
    - lector-de-codigo-de-barras
creator: dern
---

Ligero y flexible, ideal para todos los negocios que necesiten una solución a sus necesidades de POS.

| Modelo | BESC3 |
| ------ | ----------- |
| Lectura de láser   | Bi-direccional |
| Velocidad de lectura   | 100/s |
| Distancia de lectura    | 2.5 ~ 600 mm (100% UPC/EAN) |
| Interface    | USB - Cable extraible |
| Base    | Ajustable con lector removible |
