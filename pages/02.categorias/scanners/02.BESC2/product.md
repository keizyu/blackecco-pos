---
title: 'Scanner BESC2'
product:
    sku: Y10008002
    name: 'Scanner BESC2'
    bullet: 'Lector de código de barras'
    specs:
        'Modelo'                            : 'BESC2'
        'Lectura de láser'                  : 'Bi-direccional'
        'Velocidad de lectura'              : '200/s'
        'Interface'                         : 'USB - Cable extraíble '
        'Visible'                           : '650nm'
        'Distancia de lectura'              : '2.5 ~ 600 mm (100% UPC/EAN)'
        'Color'                             : 'Negro'
        'Base'                              : 'Ajustable con lector removible. '
        'Contraste de impresión mínimo'     : '30%'
        'Indicadores'                       : 'Beeper, LED'
        'Capacidad de decodificación'       : 'UPC/EAN, UPC/EAN with Supplementals, UCC/EAN 128, Code 39, Code 39 Full ASCII, Trioptic Code 39, Code 128, Code 128 Full ASCII, Codabar, Interleaved 2 of 5, Discrete 2 of 5, Code 93, MSI, Code 11, RSS variants, Chinese 2 of 5, MSI/Plessey, UK/Plessey, UCC/EAN 128, Chinese code, GS1 DataBar (RSS)series'
        'Parámetros eléctricos'             : ''
        'Volaje'                            : '5V DC±5%'
        'Tipo de láser'                     : 'EN60825-1, Clase 1'
        'Color de Led'                      : 'Inclinación 65° Elevación 60°'
        'Parámetros físicos'                : ''
        'Peso'                              : '235 g'
        'Dimensiones'                       : '156 x 68 x 83 mm (Largo, Ancho, Alto)'
        'Detalle de funcionamiento'         : ''
        'Material y resistencia'            : 'ABS+PC'
        'Temperatura de operación'          : '0℃~40℃ '
        'Temperatura en reposo'             : '-40℃~60℃'
        'Normas'                            : 'NA'
    pdf: 'ficha_lector-BESC2.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_besc2.pdf
    

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: lector-de-codigo-de-barras
  tag:
    - lector-de-codigo-de-barras
creator: dern
---

Mayor velocidad en un lector ligero que se transforma en una mayor eficiencia.

| Modelo | BESC2 |
| ------ | ----------- |
| Lectura de láser   | Bi-direccional |
| Velocidad de lectura   | 200/s |
| Distancia de lectura    | 2.5 ~ 600 mm (100% UPC/EAN) |
| Interface    | USB - Cable extraible |
| Base    | Ajustable con lector removible |
