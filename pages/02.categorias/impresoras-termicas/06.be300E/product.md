---
title: 'Impresora BE300E'
product:
    sku: Y10008003
    name: 'Impresora BE300E'
    bullet: 'Impresora termica de Tickets'
    specs:
        'Modelo': 'BE300E'
        'Velocidad': '300 mm/s'
        'Resolución de la impresión': '576 dots/line'
        'Interface': 'USB/  LAN (ETHERNET) (RJ11)'
        'Consumible': 'Paper Térmico'
        'Tamaño': '80mm'
        'Vida útil': '100 km'
        'Corte': 'Automático'
        'Rendimiento del cortador': '1,000,000 operaciones'
        'Compatible SO': 'Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10/Linux/OPOS'
        'Comandos': 'UPC-A, UPC-E, EAN-13, EAN-8, CODE39, ITF, CODEBAR, CODE93, CODE128'
        'Sensores': 'Sensor de alarma de error, sensor de tapa abierta'
        'Alerta': 'Luz parpadeante / Sonido'
        'Energía': 'Entrada: CA 110 - 120V / Salida: DC 24V / 2.5A'
        'Dimensiones': '193.3x145x144 mm (Largo x ancho x alto)'
        'Peso': '1.81 kg'
    pdf: 'ficha_tecnica_BE300E-Impresoras.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_be300e.pdf
    drivers:
        'linux': driver_linux.zip
        'win': driver_windows_2.zip

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: impresoras-termicas
  tag:
    - impresoras-termicas
creator: dern
---

Nuestro equipo más resistente y versátil, para ambientes hostiles en donde la grasa y el calor están a la orden del día. Por medio de una alarma audible y de luz tu personal sabrá cuando llego una nueva orden.

| Modelo | BE300E |
| ------ | ----------- |
| Tamaño   | 80 mm |
| Velocidad   | 300 mm/s |
| Interface    | USB / LAN (ETHERNET) (RJ11) |
| Compatible SO    | Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10/Linux/OPOS |
| Corte    | Automático |
| Vida útil | 100 Km |
| Alerta    | Luz parpadeante / Sonido |
