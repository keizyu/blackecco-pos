---
title: 'Impresora BE90'
product:
    sku: Y10008000
    name: 'Impresora BE90'
    bullet: 'Impresora térmica de Tickets Compacta'
    specs:
        'Modelo': 'BE90'
        'Velocidad': '90 mm/s'
        'Resolución de la impresión': '384 dots/line'
        'Interface': 'USB / LAN (RJ11)'
        'Consumible': 'Paper Térmico'
        'Tamaño': '58mm'
        'Vida útil': '50 km'
        'Corte': 'Manual'
        'Rendimiento del cortador': 'NA'
        'Compatible SO': 'Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10/Linux'
        'Comandos': 'UPC-A, UPC-E, EAN-13, EAN-8, CODE39, ITF, CODEBAR, CODE93, CODE128'
        'Sensores': 'NA'
        'Alerta': 'NA'
        'Energía': 'Entrada: CA 110 - 120V / Salida: DC 12V / 2.6A'
        'Dimensiones': '235x145x125 mm  (Largo x ancho x alto)'
        'Peso': '.94 kg'
    pdf: 'ficha_tecnica_BE90-Impresoras.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_be90.pdf
    drivers:
        'linux': driver_linux.zip
        'win': driver_windows.zip

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: docs
  tag:
    - impresoras-termicas
creator: dern
---

Ideal para los que necesitan una impresora confiable con un diseño compacto.

| Modelo | BE90 |
| ------ | ----------- |
| Tamaño   | 58 mm |
| Velocidad   | 90 mm/s |
| Interface    | USB / LAN (RJ11) |
| Compatible SO    | Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10/Linux |
| Corte    | Manual |
| Vida útil | 50 Km |
| Alerta    | NA |
