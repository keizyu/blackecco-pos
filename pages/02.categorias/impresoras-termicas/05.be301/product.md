---
title: 'Impresora BE301'
product:
    sku: Y10008003
    name: 'Impresora BE301'
    bullet: 'Impresora termica de Tickets'
    specs:
        'Modelo': 'BE301'
        'Velocidad': '300 mm/s'
        'Resolución de la impresión': '203 dpi'
        'Interface': 'USB / SERIAL / PARALELO/  LAN (RJ45)'
        'Consumible': 'Paper Térmico'
        'Tamaño': '80mm'
        'Vida útil': '100 km'
        'Corte': 'Automático'
        'Rendimiento del cortador': '1,000,000 operaciones'
        'Compatible SO': 'Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10'
        'Comandos': 'UPC-A, UPC-E, EAN-13, EAN-8, CODE39, ITF, CODEBAR, CODE93, CODE128'
        'Sensores': 'Sensor de alarma de error, sensor de tapa abierta'
        'Alerta': 'NA'
        'Energía': 'Entrada: CA 110 - 120V / Salida: DC 24V / 2.5A'
        'Dimensiones': '195x145x148 mm (Largo x ancho x alto)'
        'Peso': 'NA'
    pdf: 'ficha_tecnica_BE201-Impresoras.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_be301.pdf
    drivers:
        'linux': driver_windows.zip
        'win': driver_windows.zip
swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: impresoras-termicas
  tag:
    - impresoras-termicas
creator: dern
---

Alta precisión y velocidad para los ambientes más demandantes que buscan una solución robusta y confiable.

| Modelo | BE301 |
| ------ | ----------- |
| Tamaño   | 80 mm |
| Velocidad   | 300 mm/s |
| Interface    | USB / LAN (RJ11) |
| Compatible SO    | Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10 |
| Corte    | Automático |
| Vida útil | 100 Km |
| Alerta    | NA |
