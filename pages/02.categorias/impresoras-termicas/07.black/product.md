---
title: 'Impresora Black'
product:
    sku: Y10008003
    name: 'Impresora Black'
    bullet: 'Impresora termica de Tickets'
    specs:
        'Modelo'                        : 'Black'
        'Velocidad'                     : '300 mm/s'
        'Resolución de la impresión'    : '512 dots/line'
        'Interface'                     : 'USB/ Serial / LAN (RJ45) (RJ11)'
        'Consumible'                    : 'Paper Térmico'
        'Tamaño'                        : '80mm'
        'Vida útil'                     : '100 km'
        'Corte'                         : 'Automático'
        'Rendimiento del cortador'      : '1,500,000 operaciones'
        'Compatible SO'                 : 'Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10'
        'Comandos'                      : 'UPC-A, UPC-E, EAN-13, EAN-8, CODE39, ITF, CODEBAR, CODE93, CODE128'
        'Sensores'                      : 'Sensor de alarma de error, sensor de tapa abierta'
        'Alerta'                        : 'Luz parpadeante / Sonido '
        'Energía'                       : 'Entrada: CA 110 - 120V / Salida: DC 24V / 2.5A'
        'Dimensiones'                   : ' 197×136×141 mm (Largo x ancho x alto)'
        'Peso'                          : '1.8 kg'
    pdf: 'ficha_tecnica_BLACK-Impresoras.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
    drivers:
        'mac': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        'windows': 'Lorem ipsum dolor sit amet, consectetur adipiscelit.'

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: impresoras-termicas
  tag:
    - impresoras-termicas
creator: dern
---

Productividad, velocidad y seguridad, es lo que hace uno de nuestros mejores equipos. Cuenta con alarmas sonoras y de luz.

| Modelo | Black |
| ------ | ----------- |
| Tamaño   | 80 mm |
| Velocidad   | 300 mm/s |
| Interface    | USB / Serial / LAN (RJ45) (RJ11) |
| Compatible SO    | Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10 |
| Corte    | Automático |
| Vida útil | 100 Km |
| Alerta    | Luz parpadeante / Sonido |
