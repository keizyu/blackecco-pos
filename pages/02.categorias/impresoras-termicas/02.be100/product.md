---
title: 'Impresora BE100'
product:
    sku: Y10008001
    name: 'Impresora BE100'
    bullet: 'Impresora termica de Tickets'
    specs:
        'Modelo': 'BE100'
        'Velocidad': '200 mm/s'
        'Resolución de la impresión': '576 dots/line'
        'Interface': 'USB/ Serial / LAN (RJ11)'
        'Consumible': 'Paper Térmico'
        'Tamaño': '80mm'
        'Vida útil': '100 km'
        'Corte': 'Automático'
        'Rendimiento del cortador': '1,000,000 operaciones'
        'Compatible SO': 'Drivers Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 8.1/Android/IOS/Win10'
        'Comandos': 'UPC-A, UPC-E, EAN-13, EAN-8, CODE39, ITF, CODEBAR, CODE93, CODE128'
        'Sensores': 'Sensor de fin de papel, sensor de alarma de error, sensor de tapa abierta, orden de alarma'
        'Alerta': 'NA'
        'Energía': 'Entrada: CA 110 - 120V / Salida: DC 24V / 2.5A'
        'Dimensiones': '195x140x142.5  mm  (Largo x ancho x alto)'
        'Peso': '1.1 kg'
    pdf: 'ficha_tecnica_BE100-Impresoras.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_be100.pdf
    drivers:
        'linux': driver_linux.zip
        'win': driver_windows.zip
swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: impresoras-termicas
  tag:
    - impresoras-termicas
creator: dern
---

Diseño compacto con alta eficiencia que cubre las necesidades de todo comercio pequeño y mediano.

| Modelo | BE100 |
| ------ | ----------- |
| Tamaño   | 80 mm |
| Velocidad   | 200 mm/s |
| Interface    | USB / Serial / LAN (RJ11) |
| Compatible SO    | Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 8.1/Win 10/Android/iOS |
| Corte    | Automático |
| Vida útil | 100 Km |
| Alerta    | NA |
