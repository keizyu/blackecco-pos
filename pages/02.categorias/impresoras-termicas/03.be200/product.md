---
title: 'Impresora BE200'
product:
    sku: Y10008002
    name: 'Impresora BE200'
    bullet: 'Impresora termica de Tickets'
    specs:
        'Modelo': 'BE200'
        'Velocidad': '300 mm/s'
        'Resolución de la impresión': '576 dots/line'
        'Interface': 'USB/ Serial / LAN (RJ45) (RJ11)'
        'Consumible': 'Paper Térmico'
        'Tamaño': '80mm'
        'Vida útil': '100 km'
        'Corte': 'Automático'
        'Rendimiento del cortador': '1,000,000 operaciones'
        'Compatible SO': 'Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10/Linux/OPOS'
        'Comandos': 'UPC-A, UPC-E, EAN-13, EAN-8, CODE39, ITF, CODEBAR, CODE93, CODE128'
        'Sensores': 'Sensor de fin de papel, sensor de alarma de error, sensor de tapa abierta, orden de alarma'
        'Alerta': 'NA'
        'Energía': 'Entrada: CA 110 - 120V / Salida: DC 24V / 2.5A'
        'Dimensiones': '194.5x145x147  mm  (Largo x ancho x alto)'
        'Peso': '1.45 kg'
    pdf: 'ficha_tecnica_BE200-Impresoras.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_be200.pdf
    drivers:
        'linux': driver_linux.zip
        'win': driver_windows.zip
swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: impresoras-termicas
  tag:
    - impresoras-termicas
creator: dern
---

Alto rendimiento y velocidad para quienes buscan eficiencia en un ambiente demandante.

| Modelo | BE200 |
| ------ | ----------- |
| Tamaño   | 80 mm |
| Velocidad   | 300 mm/s |
| Interface    | USB / Serial / LAN (RJ45) (RJ11) |
| Compatible SO    | Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8/Win 10/Linux/OPOS |
| Corte    | Automático |
| Vida útil | 100 Km |
| Alerta    | NA |
