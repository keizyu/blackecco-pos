---
title: 'Terminal PDV'
product:
    sku: Y10008000
    name: 'Terminal PDV'
    bullet: 'Punto de venta AIO Touch 15"'
    specs:
        'Modelo'                            : 'BED701'
        'Procesador'                        : 'Intel J1900 Quad-Core 2.0GHz'
        'Memoria RAM'                       : '4GB DDR3'
        'Almacenamiento'                    : '64GB mSATA SSD'
        'Display Touch'                     : '15" TFT LED (1024x768). Receptivo 5 puntos'
        'Sistemas operativos compatibles'   : 'Windows, Linux'
        'Puertos'                           : '1 x 12V DC in <br> 1 x DB-15 VGA <br> 1 x HDMI <br> 1 x RJ-45 LAN <br> 4 x COM <br> 1 x USB 3.0 <br> 5 x USB 2.0 <br> 1 x Mic in <br> 1 x Speaker out'
        'Terminado'                         : 'Plástico resistente de alta calidad'
        'Color'                             : 'Negro'
        'Alimentación'                      : '120 v con adaptador 12V 5A'
        'Peso'                              : '6.6 Kg '
        'Dimensiones del paquete'           : '425 x 405 x 260 mm'
        'Operación / Humedad de almacenamiento' : '20% - 80% / 20% - 80%'
        'Operación / Temperatura de almacenamiento' : '0°C - 40°C / -20°C - 60°C'
    pdf: 'ficha_POS_AIO.pdf'
    media:
        thumb: product.jpg
        gallery:
            -
                url: product.jpg
            -
                url: product-2.jpg
            -
                url: product-3.jpg
    manual: manual_bepos15.pdf
    drivers:
        'linux': bepos15_lin.zip
        'win': bepos15_win.zip

swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
taxonomy:
  category: lector-de-codigo-de-barras
  tag:
    - lector-de-codigo-de-barras
creator: dern
---

La solución ideal para quien busque un equipo confiable, resistente y de gran calidad. Con almacenamiento de estado sólido y pantalla táctil.

| Modelo | BEPOS15 |
| ------ | ----------- |
| Procesador   | Intel J1900 Quad-Core 2.0GHz |
| Memoria Ram   | 4GB DDR3 |
| Almacenamiento    | 64GB mSATA SSD |
| Display Touch    | 15" TFT LED (1024x768). Receptivo 5 puntos |
| Sistemas operativos compatibles    | Windows,Linux |
| Puertos    | 1 x 12V DC in / 1 x DB-15 VGA / 1 x HDMI / 1 x RJ-45 LAN / 4 x COM / 1 x USB 3.0 / 5 x USB 2.0 / 1 x Entrada de micrófono / 1 x Salida de audio  |
