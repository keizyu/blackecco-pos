---
title: Política de Cookies
body:
  attr:
  class:
    - 'pg-privacidad'
    - default
category:
  title: Política de Cookies
---

Durante su visita a esta página, “**BlackEcco**” en automático podrá recabar la siguiente información referente a de su computadora: (1) dominio y servidor del cual acceda a internet; (2) dirección de internet respecto de la página de la que se enlaza directamente a esta página, en su caso, fecha y hora en la que usted ingresó a nuestra página y cuánto tiempo permaneció en esta misma así como qué áreas visitó; (3) dirección del protocolo de internet (IP); y (4) sistema operativo de su equipo de cómputo y software de su navegador.

Nuestra página de internet puede utilizar cookies, los cuales envían información a su equipo de cómputo mientras navega en esta página; las cookies son únicas a su equipo de cómputo y permiten al o los servidores de internet poder recabar información que harán que su uso de esta página sea más favorable y accesible. Las cookies le permiten economizar tiempo cuando regresa a esta página y solamente pueden ser registrados por su servidor de internet en el dominio que le emitió la cookie. Cabe aclarar que las cookies no pueden ser utilizados para operar programas informáticos o para ingresar virus a su equipo de cómputo.

Se utilizan cookies para recabar información no personal de los visitantes en línea, así mismo permiten identificar y rastrear el tipo de navegador, sistema operativo y el servicio de internet que utiliza, permitiendo tabular el total de visitantes que ingresan a nuestra página. Sin embargo, los visitantes pueden desactivar las cookies en su equipo de cómputo o programarle para notificarle cuando se le llegan cookies por medio del icono de preferencias en su navegador.

Los datos personales que recabe “**BlackEcco**”, se administrarán y se tratarán conforme a nuestro aviso de privacidad, usándolos enunciativamente entre otros, para: (1) ubicarlo; (2) identificarlo; (3) contactarlo; (4) enviarle publicidad, avisos, invitaciones, información y/o mercancía; (5) finalidades estadísticas; (6) atender sus comentarios; y (7) dar contestación a solicitudes de trabajo.
