---
title: Política de privacidad
body:
  attr:
  class:
    - 'pg-privacidad'
    - default
category:
  title: Aviso de Privacidad
---

### AVISO DE CONFIDENCIALIDAD

La información de los correos electrónicos de **BlackEcco Group**, incluyendo sus archivos adjuntos, son personales, privilegiados y confidenciales, y se envían única y exclusivamente a la persona y/o entidad a quien va dirigido. La difusión, distribución y/o reproducción de dicha información confidencial sin consentimiento por escrito del remitente está prohibida. Si usted no es el destinatario al que este correo está dirigido, favor de contactar al remitente respondiendo al presente correo y eliminar el correo original incluyendo sus archivos. Queda usted notificado que su difusión, distribución o el copiado de este correo y de sus archivos adjuntos está prohibido, debiendo responder por los daños y perjuicios que le ocasione al remitente. 

### AVISO DE PRIVACIDAD Y TRATAMIENTO DE DATOS PERSONALES

BLACK ECCO, S.A. DE C.V., con domicilio en Privada Corina número 18, Colonia del Carmen, Delegación Coyoacán, C.P. 04100, Ciudad de México (en lo sucesivo “**BlackEcco**”), es consciente de la importancia que tiene el tratamiento controlado de sus datos personales, por ello “**BlackEcco**” pone a su disposición este “**Aviso de Privacidad**”, con la finalidad de que usted (persona física o moral) como “**Titular**” de la información (Datos Personales), conozca su práctica al usar, divulgar, obtener o almacenar sus datos personales, de conformidad con la **LEY FEDERAL DE PROTECCIÓN DE DATOS PERSONALES EN POSESIÓN DE LOS PARTICULARES** (en lo sucesivo “**La Ley**”).

**Datos Personales:** **“BlackEcco”**, podrá recabar y obtener los siguientes datos personales de usted:

* **Persona física:**

	1. Identificación: Nombre, fecha de nacimiento, edad, género, nacionalidad, domicilio, estado civil, credencial para votar (INE), Registro Federal de Contribuyentes (RFC), Clave Única de Población (CURP), Número de Seguridad Social (NSS), teléfono fijo y móvil, fotografías, firma y voz;
  2. Académicos: Datos educativos de su educación, idiomas, aptitudes y habilidades;
  3. Laborales: Curriculum Vitae (CV), grado de estudios, nivel de idiomas, experiencias, antecedentes y referencias laborales (cartas de recomendación);
  4. Económicos: Estudios Socioeconómicos;
  5. Videograbaciones de video y/o voz: Grabaciones de entrevistas;
  6. Datos de terceros: Datos de referencia personales;
  7. Se le podrá requerir la siguiente información sensible: Historial Crediticio y Constancia de No Antecedentes Penales.

* **Persona Moral:**

  1. Acta Constitutiva, en su caso, Instrumentos Notariales de Reformas y Modificaciones;
  2. Registro de inscripción al Registro Público de la Propiedad y del Comercio;
  3. Poder del Representante Legal o Apoderado;
  4. Registro Federal de Contribuyentes (RFC);
  5. Comprobante de domicilio;
  6. Se le podrá requerir la siguiente información sensible: Historial Crediticio y Datos Bancarios (en caso de transferencias).


**Proceso:** Sus datos personales a excepción de los datos personales sensibles, podrán ser recabados y tratados a través de medios de “**BlackEcco**” o a través de plataformas o software que permitan el intercambio de texto, voz, video por internet etc., por parte de “**BlackEcco**”. Es responsabilidad del “**Titular**” garantizar que sus datos proporcionados son veraces y comunicar la actualización o modificación de estos mismos.  

Así mismo, durante su visita a esta página, “**BlackEcco**” en automático podrá recabar la siguiente información referente a de su computadora: (1) dominio y servidor del cual acceda a internet; (2) dirección de internet respecto de la página de la que se enlaza directamente a esta página, en su caso, fecha y hora en la que usted ingresó a nuestra página y cuánto tiempo permaneció en esta misma así como qué áreas visitó; (3) dirección del protocolo de internet (IP); y (4) sistema operativo de su equipo de cómputo y software de su navegador. Nuestra página de internet puede utilizar cookies, los cuales envían información a su equipo de cómputo mientras navega en esta página; las cookies son únicos a su equipo de cómputo y permiten al o los servidores de internet poder recabar información que harán que su uso de esta página sea más favorable y accesible. Las cookies le permiten economizar tiempo cuando regresa a esta página y solamente pueden ser registrados por su servidor de internet en el dominio que le emitió la cookie. Cabe aclarar que las cookies no pueden ser utilizados para operar programas informáticos o para ingresar virus a su equipo de cómputo.

Se utilizan cookies para recabar información no personal de los visitantes en línea, así mismo permiten identificar y rastrear el tipo de navegador, sistema operativo y el servicio de internet que utiliza, permitiendo tabular el total de visitantes que ingresan a nuestra página. Sin embargo, los visitantes pueden desactivar las cookies en su equipo de cómputo o programarle para notificarle cuando se le llegan cookies por medio del icono de preferencias en su navegador.

Los datos personales que recabe “**BlackEcco**”, se administrarán y se tratarán conforme a este aviso de privacidad, usándolos enunciativamente entre otros, para: (1) ubicarlo; (2) identificarlo; (3) contactarlo; (4) enviarle publicidad, avisos, invitaciones, información y/o mercancía; (5) finalidades estadísticas; (6) atender sus comentarios; y (7) dar contestación a solicitudes de trabajo.

Al visitar esta página y/o aceptar ser contactado, se estará consintiendo expresamente al tratamiento de datos personales de conformidad con este “**Aviso de Privacidad**” y en su caso, con el aviso de privacidad de los proveedores de servicios de las plataformas utilizadas.

**Tratamiento:** Sus datos personales que nos proporcione y/o recabemos, incluyendo datos sensibles, serán tratados para fines comerciales y elaboración de cotizaciones, contratos, convenios, órdenes de compra o cualquier otro documento.

**Transferencia:** “**BlackEcco**” podrá transferir sus datos personales a personas físicas o morales relacionadas a **BlackEcco Group**, accionistas, afiliadas o controladoras, así como a cualquier tercero relacionado comercialmente con “**BlackEcco**” y en los casos previstos en la Ley.

**Derechos:** Como titular de los datos personales, usted podrá ejercer ante el Departamento Jurídico de “**BlackEcco**”, los derechos de acceso, rectificación, cancelación y oposición (derechos “**ARCO**”), establecidos en “**la Ley**”, pudiendo revocar o limitar en todo momento, el consentimiento que haya otorgado para el tratamiento de sus datos personales. Dicha revocación o limitación podrá efectuarla conforme a las disposiciones de “**La Ley**” al correo electrónico black@blackecco.com.

**Modificaciones:** “**BlackEcco**” podrá modificar el presente aviso de privacidad y tratamiento de datos personales, según convenga, los cuales, serán publicados y comunicados en la página de internet www.blackeccogroup.com, en la sección de Aviso de Privacidad.

**Consentimiento:** Al proporcionar sus datos personales voluntarios y sus datos personales sensibles, está consintiendo su tratamiento en términos del presente “**Aviso de Privacidad**”, incluyendo su consentimiento para realizar transferencias de dicha información, pudiendo solicitar en cualquier momento que sus documentos sean baja de nuestra base de datos. La temporalidad del manejo de los datos personales del “**Titular**” será por el tiempo razonable derivado del vínculo que se origine entre el “**Titular**” y “**BlackEcco**”. **Última Actualización 15 de noviembre del 2017.**
