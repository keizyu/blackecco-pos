---
container:
  attr:
    id: top
  class:
    - page-section-top
    - page-top-soporte
  is_fluid: false
---

## soporte técnico
<div class="wow fadein">En esta sección encontraras las preguntas frecuentes acerca de nuestros equipos.</div>
