---
container:
  attr:
    id: black-banner
  class:
    - page-section
    - page-section-black
  is_fluid: false


grid:
    -
        class:
          - justify-content-center
        cols:
            -
              class:
                - col-md-9
              content: |
                ## <span> ¿Deseas contactarnos? </span>
    -
        class:
          - justify-content-center
        cols:
            -
              class:
                - col-sm-4
                - offset-sm-1
                - contact-banner-mail
              content: |
                support@blackeccopos.com
            -
              class:
                - col-sm-4
                - offset-sm-1
                - contact-banner-tel
              content: |
                01 800 8012667
---
