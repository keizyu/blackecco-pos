---
container:
  attr:
    id: ayuda
  class:
    - page-section
  is_fluid: false

accordion:
    left:
      title: 'impresoras'
      faq:
        '¿Por qué no puedo imprimir?' : 'La impresora está apagada. <br> El cable USB está desconectado de la impresora o del ordenador. <br> La impresora no tiene papel. <br> El cable de poder está desconectado.'
        '¿Por qué mi impresora da muchos pitidos?' : 'Depende el modelo de tu impresora, esta dará pitidos para alertarnos de diferentes situaciones, la principal es la ausencia de papel o que la tapa no está bien cerrada. <br> En el caso de la impresora BE-300, emite pitidos cada que se imprime algo y esto es configurable a través de una aplicación.'
        '¿De qué manera instalo el papel de mi impresora?' : 'Todo rollo de papel térmico tiene un derecho y un revés, debemos asegurarnos de que el lado térmico del papel quede enfrente, como en la siguiente imagen. <p style="margin-top: 0; padding: 0; text-align:center"><img src="http://blackecco.com/wp-content/uploads/2016/02/papel.png"</p>'
        '¿Dónde puedo recibir ayuda en caso de no poder instalar mi impresora? ' : 'Tenemos apoyo de soporte técnico, una vez que registres tu producto puedes contactarnos, nuestros datos están en la página <a href="/contacto" target="_blank">http://blackeccopos.com/contacto/</a>'
        '¿Qué tipo de papel usa mi impresora? ' : 'El tipo de papel que soporta la impresora depende del modelo y en todos los casos es papel térmico, el tamaño lo puedes revisar en la siguiente tabla:<table style="margin: 0 auto;"><tbody><tr><td><strong>Modelo</strong></td><td><strong>Tamaño de papel</strong></td></tr><tr><td><strong>BE-90</strong></td><td>58mm</td></tr><tr><td><strong>BE-95</strong></td><td>58mm</td></tr><tr><td><strong>BE-100</strong></td><td>80mm</td></tr><tr><td><strong>BE-200</strong></td><td>80mm</td></tr><tr><td><strong>BE-300</strong></td><td>80mm</td></tr></tbody></table>'
        '¿Con qué nombre aparece mi impresora en dispositivos? ' : 'Aparece con el nombre de POS-XXX o XP-XXX, donde las “X” pueden variar dependiendo del modelo de impresora.'
        '¿Por qué mi impresora realiza la función de impresión y expulsa el papel,<br> pero no aparece nada impreso?' : 'El motivo puede ser que el papel térmico está mal instalado. Verifica el sentido y que sea papel térmico.'
        '¿Cuáles son los sistemas operativos compatibles con mi impresora?' : 'Los sistemas operativos compatibles son Win 9X/Win ME/Win 2000/Win 2003/Win NT/Win XP/Win Vista/Win 7/Win 8 /Win 10/Linux'
        '¿Cómo instalo el driver de mi impresora?' : '<div class="responsive-video"><iframe src="https://www.youtube.com/embed/X2tNi-EqolQ" width="300" height="150" frameborder="0"></iframe></div>'
    right:
      title: 'FAQS'
      faq:
        'Mi equipo presenta un problema de instalación o funcionamiento.' : 'Si deseas asesoría técnica acerca de como instalar tu equipo, descagar drivers o tienes algún problema de funcionamiento, te invitamos a contactarnos vía telefónica: Lunes a Viernes al número 018008012667 Ext 401, o puedes escribirnos al correo <a href="mailto:soporte@blackeccopos.com">soporte@blackeccopos.com</a> y en breve nos pondremos en contacto contigo. Es importante que estes cerca de tu equipo. '
        '¿Cómo hago valida mi garantía?' : 'Si tu equipo presenta una falla y en soporte técnico no lograron darte una solución adeacuada, te pedimos que acudas con tu distribuidor para realizar un breve diagnóstico y en menos de 24 horas se realice el cambio físico. El cambio está sujeto a defectos de fábrica y no cubre daños ocasionados por golpes o mal uso del equipo.'
        'Quiero ser un distribuidor mayorista.' : 'Por favor comunícate por correo electrónico a <a href="mailto:contacto@blackeccopos.com">contacto@blackeccopos.com</a> y en breve nos comunicaremos contigo.'
        'Tengo un proyecto de volumen y necesito asesoria personalizada.' : 'Trabajamos de la mano con los mayoristas y en conjunto podemos brindarte asesoría y soluciones para cualquier tipo de proyecto. No dudes en acercarte a nosostros y contarnos cuales son tus necesidades tecnológicas, juntos encontraremos la mejor solución.'

---

## ¿Te podemos ayudar en algo?
