---
title: "Home"
menu: Home
body:
  attr: {}
  class:
    - be-home

metadata:
  robots: 'index, follow'

content:
  items:
    - '@self.modular'
  order:
    by: default
    dir: asc
    custom:
      - _top
      - _categorias
      - _destacados
      - _garantia
      - _donde
      - _soluciones
---
