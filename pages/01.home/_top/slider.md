---
container:
  attr:
    id: encabezado
  class:
    - page-header
  innercontainer_class: ['top']
  is_fluid: false


swiper:
  as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
  slides:
    -
      name: Slider 1
      class: []
      template: partials/slide/slider.twig
      content:
        media:
          src: producto-nuevo.png
        banner:
          src: slider-home.jpg
          src2x: slider-home@2x.jpg
        content: |
          #### <span>BIENVENIDO A BLACK ECCO POS</span>

          ## TENEMOS LA MEJOR SELECCIÓN DE PRODUCTOS PARA PDV
          #### <span>IMPRESORAS, LECTORES, CAJONES, TERMINALES Y MÁS.</span>
          [Descubre más](/categorias?classes=button)

    -
      name: Slider 2
      class: []
      template: partials/slide/slider.twig
      content:
        media:
          src: producto-nuevo-2.png
        banner:
          src: slider-home-2.jpg
          src2x: slider-home-2@2x.jpg
        content: |
          #### <span>BENEFICIOS Black Ecco POS</span>

          ## GARANTIA EXPRESS <br>24 HORAS

          [Descubre más](/garantia?classes=button)
    -
      name: Slider 3
      class: []
      template: partials/slide/slider.twig
      content:
        banner:
          src: slider-home-3.jpg
          src2x: slider-home-3@2x.jpg
        content: |
          #### <span>DISTRIBUIDORES</span>

          ## AQUÍ PUEDES ENCOTRAR TODOS NUESTROS PRODUCTOS
          ![DC Mayorista](/user/pages/01.home/_top/distribuidores/dcm-logo.png)
          ![DC Mayorista](/user/pages/01.home/_top/distribuidores/ct-logo.png)
          ![DC Mayorista](/user/pages/01.home/_top/distribuidores/cva-logo.png)

---
