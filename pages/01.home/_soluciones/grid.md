---
container:
  attr:
    id: blue-banner
  class:
    - page-section
    - page-section-blue
  is_fluid: false


grid:
  - class:
      - justify-content-around
    cols:
      - class:
          - col-md-9
        content: |
          ## <span class="wow fadein" > brindamos <strong>soluciones en tecnología</strong> a la medida de su negocio </span>
---
