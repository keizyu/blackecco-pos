---
container:
  attr:
    id: slider-marcas
  class:
    - page-section
    - page-section-marcas
    - border-bottom
  innercontainer_class: ['carousel']
  is_fluid: false


swiper:
  #as_bkg: true
  container:
    class: []
    attr: {}
  nav:
    prev:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE002;'
    next:
      class:
        - ico
        - ico-md
      attr:
        data-ico: '&#xE003;'
  slides:
    -
      name: Slider 1
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: featured-1.jpg
        content: |
          #### <span>Impresora BE90</span>

          <span class="bullet"> Ideal para los que necesitan una impresora confiable con un diseño compacto.</span>

          [ver producto](/categorias/impresoras-termicas/be90?classes=button)
    -
      name: Slider 2
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: featured-2.jpg
        content: |
          #### <span>Impresora BE100</span>

          <span class="bullet"> Diseño compacto con alta eficiencia que cubre las necesidades de todo comercio pequeño y mediano. </span>

          [ver producto](/categorias/impresoras-termicas/be100?classes=button)
    -
      name: Slider 3
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: featured-3.jpg
        content: |
          #### <span>Impresora BE200</span>

          <span class="bullet"> Alto rendimiento y velocidad para quienes buscan eficiencia en un ambiente demandante.</span>

          [ver producto](/categorias/impresoras-termicas/be200?classes=button)
    -
      name: Slider 4
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: featured-4.jpg
        content: |
          #### <span>Impresora BE300</span>

          <span class="bullet">Nuestro equipo más resistente y versátil, para ambientes hostiles en donde la grasa y el calor están a la orden del día. </span>

          [ver producto](/categorias/impresoras-termicas/be300?classes=button)
    -
      name: Slider 5
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: featured-5.jpg
        content: |
          #### <span>Impresora BE300E</span>

          <span class="bullet">Nuestro equipo más resistente y versátil, para ambientes hostiles en donde la grasa y el calor están a la orden del día.</span>

          [ver producto](/categorias/impresoras-termicas/be300e?classes=button)
    -
      name: Slider 6
      class: []
      template: partials/slide/slider-productos.twig
      content:
        media:
          src: featured-6.jpg
        content: |
          #### <span>Impresora Black</span>

          <span class="bullet">Productividad, velocidad y seguridad es lo que hace uno de nuestros mejores equipos. Sus alarmas sonoras y de luz.</span>

          [ver producto](/categorias/impresoras-termicas/black?classes=button)
---
### Productos Destacados
 Nuestra selección de artículos recomendados.
