<?php
/**
 * BLACKECCO POS LOGIC
 */



namespace Grav\Theme;

use Grav\Common\Data\Data;
use Grav\Common\Page\Header;
use Grav\Common\Page\Page;
use Grav\Common\Page\Pages;
use Grav\Common\Session;
use Grav\Common\Theme;
use Grav\Common\Utils;

class Rentec extends Theme
{
	protected $data;
	protected $products;




	public static function getSubscribedEvents()
	{
		return [
			'onPageInitialized' => ['onPageInitialized', 0],
			'onFormValidationProcessed' => ['onFormValidationProcessed', 0]
		];
	}


	public function onPageInitialized()
	{
		$uri = $this->grav['uri'];
		$cart_route = $this->config->get('site.catalog.cart_route');

		if($uri->path() == $cart_route)
		{
			$this->products();
		}
	}


	public function products()
	{
		// get session data
		if( ! $this->grav['session']->products)
		{
			$this->grav['session']->products = array();
		}

		// get form data
		if (isset($_POST) && isset($_POST['form-nonce']) && isset($_POST['action']))
		{
			$this->data = new Data($_POST);

			if(Utils::verifyNonce($this->data->get('form-nonce'), $this->config->get('site.catalog.nounce_action')))
			{
				switch($this->data->get('action'))
				{
					case 'add':
						$this->addProduct();
						break;

					case 'qty':
						$this->quantityProduct();
						break;

					case 'delete':
						$this->deleteProduct();
						break;

					default:
						//nothing here!!
						break;
				}
			}
		}


		// get the saved products
		$this->grav['page']->modifyHeader('cart_products', $this->grav['session']->products);
	}


	public function addProduct()
	{
		$this->data = new Data($_POST);

		if($this->data->get('product'))
		{
			$product = strip_tags($this->data->get('product'));
			$page = $this->grav['page']->find($product);

			// save it in the session
			if($page)
			{
				$products = $this->grav['session']->products;

				// check if the the product is already added
				if(array_key_exists($product, $products))
				{
					$products[$product] = (int) $this->data->get('quantity') ? (int) $this->data->get('quantity') : $products[$product] + 1;
				}

				else
				{
					$products[$product] = (int) $this->data->get('quantity') ? (int) $this->data->get('quantity') : 1;
				}

				// add the url of the product
				$this->grav['session']->products = $products;
			}

			else
			{
				var_export('flash!!');
			}
		}
	}

	public function quantityProduct()
	{
		$this->addProduct();

		$response = array(
			"success" => true
		);

		echo json_encode($response);
		die();
	}

	public function deleteProduct()
	{
		if($this->data->get('product'))
		{
			$product = strip_tags($this->data->get('product'));
			$page = $this->grav['page']->find($product);

			// save it in the session
			if($page)
			{
				$products = $this->grav['session']->products;
				unset($products[$product]);
				$this->grav['session']->products = $products;
			}

			$response = array(
				"success" => true
			);

			echo json_encode($response);
			die();
		}
	}

	public function onFormValidationProcessed($event)
	{
		$uri = $this->grav['uri'];
		$cart_route = $this->config->get('site.catalog.cart_route');

		if($uri->path() == $cart_route)
		{
			// echo '<pre>'.var_export($event['form'], true).'</pre>';
			// die();
			unset($this->grav['session']->products);
		}
	}
}
