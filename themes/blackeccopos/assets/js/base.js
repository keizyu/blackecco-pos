/* BlackEcco Site */

(function($) {

$(document).ready(function(){

	//WOW Animations
	var $animDelay = $('#donde-comprar .cel-image, #acerca-de .usp, #black-banner .row')
		$animDelay.addClass('wow fadein');
		$animDelay.eq(0).attr('data-wow-delay','0.1s');
		$animDelay.eq(1).attr('data-wow-delay','0.5s');
		$animDelay.eq(2).attr('data-wow-delay','0.9s');
		$animDelay.eq(3).attr('data-wow-delay','1.4s');
		$animDelay.eq(4).attr('data-wow-delay','1.9s');
		$animDelay.eq(5).attr('data-wow-delay','2.4s');
		$animDelay.eq(6).attr('data-wow-delay','2.9s');
		$animDelay.eq(7).attr('data-wow-delay','3.4s');
		$animDelay.eq(8).attr('data-wow-delay','3.9s');
		$animDelay.eq(9).attr('data-wow-delay','4.4s');
		$animDelay.eq(10).attr('data-wow-delay','4.9s');
		$animDelay.eq(11).attr('data-wow-delay','5.4s');

	wow = new WOW({
		mobile: false
	});
	wow.init();

	// mobile menu
	$('#main-menu').slb_mobile_drop_menu();

	// carousel-slider swiper
	$('.carousel .swiper-container').each(function(i) {
		var mySwiper = new Swiper ($('.carousel .swiper-container')[i], {

			// Navigation arrows
			nextButton: $('.carousel .swiper-button-next')[i],
			prevButton: $('.carousel .swiper-button-prev')[i],

			// Optional parameters
			cssWidthAndHeight: true,
			slidesPerView: 4,
			visibilityFullFit: true,
			autoResize: false,
			breakpoints: {
				1024: {
					slidesPerView: 4,
					spaceBetween: 40,
				},
				992: {
					slidesPerView: 2,
					spaceBetween: 30,
				},
				640: {
					slidesPerView: 1,
					spaceBetween: 20,
				}
			}
		});
	});

	// homepage swiper
	if($('#encabezado .swiper-container').length)
	{
		var homeswiper = new Swiper('#encabezado .swiper-container', {
			nextButton: '#encabezado .swiper-button-next',
			prevButton: '#encabezado .swiper-button-prev',
			pagination: '#encabezado .swiper-pagination',
			paginationClickable: true,

			//autoplay
			autoplay: 5000,
			speed: 1000,
			//watchSlidesProgress: true,
			//watchVisibility: true,

			//Loop
			loop: true,
			lazyLoading: true,
		});
	}

	//Product swiper
	if($('#product-images').length)
	{
		var productswiper = new Swiper('#product-images', {
			//onlyExternal: true,
			nextButton: '#product-images .swiper-button-next',
			prevButton: '#product-images .swiper-button-prev',
		});

		var set_thumb = function()
		{
			var target = productswiper.realIndex;
			$('#product-image-thumbs a').removeClass('active');
			$('#product-image-thumb-' + target).addClass('active');
		};


		$('#product-image-thumbs a').click(function(e)
		{
			var target = $(this).attr('data-num');
			productswiper.slideTo(target);
		});


		productswiper.on('onSlideChangeStart', set_thumb);
		set_thumb();
	}

	$('#parallax-banner').each(function() {
		$(this).attr('data-img-width', '1600').attr('data-img-height', '1064').attr('data-diff', '100');
	});


	//Tabs
	$('#tab1').change(function(){
        if(this.checked)
            $('#content1').show().siblings().hide();
        	$('.icn-1').css('background-color', '#ffffff');
        	$('.icn-2').css('background-color', '#7a8e9c');
        	$('.icn-3').css('background-color', '#5c7485');
    });

    $('#tab2').change(function(){
        if(this.checked)
            $('#content2').show().siblings().hide();
        	$('.icn-1').css('background-color', '#7a8e9c');
        	$('.icn-2').css('background-color', '#ffffff');
        	$('.icn-3').css('background-color', '#5c7485');
    });

    $('#tab3').change(function(){
        if(this.checked)
            $('#content3').show().siblings().hide();
        	$('.icn-1').css('background-color', '#5c7485');
        	$('.icn-2').css('background-color', '#7a8e9c');
        	$('.icn-3').css('background-color', '#ffffff');
    });

// ACCORDION

		(function(){
			var d = document,
			accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
			setAria,
			setAccordionAria,
			switchAccordion,
		  touchSupported = ('ontouchstart' in window),
		  pointerSupported = ('pointerdown' in window);

		  skipClickDelay = function(e){
		    e.preventDefault();
		    e.target.click();
		  }

				setAriaAttr = function(el, ariaType, newProperty){
				el.setAttribute(ariaType, newProperty);
			};
			setAccordionAria = function(el1, el2, expanded){
				switch(expanded) {
		      case "true":
		      	setAriaAttr(el1, 'aria-expanded', 'true');
		      	setAriaAttr(el2, 'aria-hidden', 'false');
		      	break;
		      case "false":
		      	setAriaAttr(el1, 'aria-expanded', 'false');
		      	setAriaAttr(el2, 'aria-hidden', 'true');
		      	break;
		      default:
						break;
				}
			};

		switchAccordion = function(e) {
		  console.log("triggered");
			e.preventDefault();
			var thisAnswer = e.target.parentNode.nextElementSibling;
			var thisQuestion = e.target;
			if(thisAnswer.classList.contains('is-collapsed')) {
				setAccordionAria(thisQuestion, thisAnswer, 'true');
			} else {
				setAccordionAria(thisQuestion, thisAnswer, 'false');
			}
		  	thisQuestion.classList.toggle('is-collapsed');
		  	thisQuestion.classList.toggle('is-expanded');
				thisAnswer.classList.toggle('is-collapsed');
				thisAnswer.classList.toggle('is-expanded');

		  	thisAnswer.classList.toggle('animateIn');
			};
			for (var i=0,len=accordionToggles.length; i<len; i++) {
				if(touchSupported) {
		      accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
		    }
		    if(pointerSupported){
		      accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
		    }
		    accordionToggles[i].addEventListener('click', switchAccordion, false);
		  }
		})();

		(function(){
			var i=0;
			$('.accordion-content').each(function(){
			    i++;
			    var newID='accordion'+i;
			    $(this).attr('id',newID);
			    $(this).val(i);
			});

			var i=0;
			$('.accordion-title').each(function(){
			    i++;
			    var newAnchor='accordion'+i;
			    $(this).attr('href','#' + newAnchor);
					$(this).attr('aria-controls',newAnchor);
			    $(this).val(i);
			});

		})();

	/* detect touch */
	if("ontouchstart" in window){
		document.documentElement.className = document.documentElement.className + " touch";
	}
	if(!$("html").hasClass("touch")){
		/* background fix */
		$("#parallax-banner").css("background-attachment", "fixed");
	}

	/* fix vertical when not overflow
	call fullscreenFix() if .fullscreen content changes */
	function fullscreenFix(){
		var h = $('body').height();
		// set .fullscreen height
		$(".content-b").each(function(i){
			if($(this).innerHeight() > h){ $(this).closest(".fullscreen").addClass("overflow");
			}
		});
	}
	$(window).resize(fullscreenFix);
	fullscreenFix();

	/* resize background images */
	function backgroundResize(){
		var windowH = $(window).height();
		$(".background").each(function(i){
			var path = $(this);
			// variables
			var contW = path.width();
			var contH = path.height();
			var imgW = path.attr("data-img-width");
			var imgH = path.attr("data-img-height");
			var ratio = imgW / imgH;
			// overflowing difference
			var diff = parseFloat(path.attr("data-diff"));
			diff = diff ? diff : 0;
			// remaining height to have fullscreen image only on parallax
			var remainingH = 0;
			if(path.hasId("parallax-banner") && !$("html").hasClass("touch")){
				var maxH = contH > windowH ? contH : windowH;
				remainingH = windowH - contH;
			}
			// set img values depending on cont
			imgH = contH + remainingH + diff;
			imgW = imgH * ratio;
			// fix when too large
			if(contW > imgW){
				imgW = contW;
				imgH = imgW / ratio;
			}
			//
			path.data("resized-imgW", imgW);
			path.data("resized-imgH", imgH);
			path.css("background-size", imgW + "px " + imgH + "px");
		});
	}
	$(window).resize(backgroundResize);
	$(window).focus(backgroundResize);
	backgroundResize();

	/* set parallax background-position */
	function parallaxPosition(e){
		var heightWindow = $(window).height();
		var topWindow = $(window).scrollTop();
		var bottomWindow = topWindow + heightWindow;
		var currentWindow = (topWindow + bottomWindow) / 2;
		$("#parallax-banner").each(function(i){
			var path = $(this);
			var height = path.height();
			var top = path.offset().top;
			var bottom = top + height;
			// only when in range
			if(bottomWindow > top && topWindow < bottom){
				var imgW = path.data("resized-imgW");
				var imgH = path.data("resized-imgH");
				// min when image touch top of window
				var min = 0;
				// max when image touch bottom of window
				var max = - imgH + heightWindow;
				// overflow changes parallax
				var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow; // fix height on overflow
				top = top - overflowH;
				bottom = bottom + overflowH;
				// value with linear interpolation
				var value = min + (max - min) * (currentWindow - top) / (bottom - top);
				// set background-position
				var orizontalPosition = path.attr("data-oriz-pos");
				orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
				$(this).css("background-position", orizontalPosition + " " + value + "px");
			}
		});
	}
	if(!$("html").hasClass("touch")){
		$(window).resize(parallaxPosition);
		//$(window).focus(parallaxPosition);
		$(window).scroll(parallaxPosition);
		parallaxPosition();
	}

});

}(jQuery));
